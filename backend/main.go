package main

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/streadway/amqp"
	"io/ioutil"
	"net/http"
	"os"
)

type uploadResponse struct {
	Status string `json:"status"`
	Msg    string `json:"message,omitempty"`
}

func sendTask(channelRabbitMQ *amqp.Channel, id string) bool {
	// Attempt to publish a message to the queue.

	message := amqp.Publishing{
		ContentType: "text/plain",
		Body:        []byte(id),
	}
	if err := channelRabbitMQ.Publish(
		"",             // exchange
		"QueueService", // queue name
		false,          // mandatory
		false,          // immediate
		message,        // message to publish
	); err != nil {
		return false
	}
	return true
}

func initialQueue() *amqp.Channel {
	// Define RabbitMQ server URL.
	//amqpServerURL := "amqp://guest:guest@localhost:5672"
	amqpServerURL := os.Getenv("AMQP_SERVER_URL")

	// Create a new RabbitMQ connection.
	connectRabbitMQ, err := amqp.Dial(amqpServerURL)
	if err != nil {
		panic(err)
	}
	//defer connectRabbitMQ.Close()

	// Let's start by opening a channel to our RabbitMQ
	// instance over the connection we have already
	// established.
	channelRabbitMQ, err := connectRabbitMQ.Channel()
	if err != nil {
		panic(err)
	}
	//defer channelRabbitMQ.Close()

	// With the instance and declare Queues that we can
	// publish and subscribe to.
	_, err = channelRabbitMQ.QueueDeclare(
		"QueueService", // queue name
		true,           // durable
		false,          // auto delete
		false,          // exclusive
		false,          // no wait
		nil,            // arguments
	)
	if err != nil {
		panic(err)
	}

	return channelRabbitMQ
}
func tokenGenerator() string {
	b := make([]byte, 6)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

func main() {
	channel := initialQueue()
	r := gin.Default()

	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"data": "imageEnhancement project"})
	})

	r.POST("/api/v1/uploadImage", func(c *gin.Context) {
		directory := os.Getenv("FROM_DIRECTORY")

		token := tokenGenerator()
		sendTask(channel, token)

		var (
			IMAGE_TYPES = map[string]interface{}{
				"image/jpeg": nil,
				"image/png":  nil,
			}
		)
		const (
			MAX_UPLOAD_SIZE = 5 << 20 // 5 megabytes
		)

		// Limit Upload File Size
		c.Request.Body = http.MaxBytesReader(c.Writer, c.Request.Body, MAX_UPLOAD_SIZE)

		file, fileHeader, err := c.Request.FormFile("file")
		if err != nil {
			c.JSON(http.StatusBadRequest, &uploadResponse{
				Status: "error1",
				Msg:    err.Error(),
			})
			return
		}

		defer file.Close()

		buffer := make([]byte, fileHeader.Size)
		file.Read(buffer)
		fileType := http.DetectContentType(buffer)

		// Validate File Type
		if _, ex := IMAGE_TYPES[fileType]; !ex {
			c.JSON(http.StatusBadRequest, &uploadResponse{
				Status: "error",
				Msg:    "file type is not supported",
			})
			return
		}

		f, err := os.Create(directory + "/" + token)
		if err != nil {
			panic(err)
		}
		defer f.Close()
		f.Write(buffer)

		c.JSON(http.StatusOK, gin.H{"data": token})
	})

	r.GET("/api/v1/getImage/:id", func(c *gin.Context) {
		id := c.Param("id")
		directory := os.Getenv("TO_DIRECTORY")

		data, err := ioutil.ReadFile(directory + "/" + id + ".jpg")
		if err != nil {
			panic(err)
		}
		encodedString := base64.StdEncoding.EncodeToString(data)
		c.JSON(http.StatusOK, gin.H{"data": encodedString})
		fmt.Print(string(data))

	})
	r.Run(":8080")
}
